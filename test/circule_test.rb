# frozen_string_literal: true

require 'test_helper'

class CirculeTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Circule::VERSION
  end

  def test_it_does_something_useful
    test_hash = '0123456789abcdef123456789abcdef023456789abcdef01456789abcdef0123'
    test_blob = Circule.new(hex: test_hash).image.to_blob
    saved_b64 =
      "UDYKMjQgMjQKMjU1Coor4oor4oor4oor4oor4oor4oor4oor4oor4oor4oor\n"\
      "4oor4oor4oor4oor4oor4qUqKqUqKqUqKqUqKgAA/wAA/wAA/wAA/4or4oor\n"\
      "4oor4oor4oor4oor4oor4oor4oor4oor4oor4oor4oor4oor4qUqKqUqKqUq\n"\
      "KqUqKqUqKqUqKqUqKgAA/wAA/wAA/4or4oor4oor4oor4oor4oor4oor4oor\n"\
      "4oor4oor4oor4oor4qUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKgAA\n"\
      "/wAA/4or4oor4oor4oor4oor4oor4oor4oor4oor4oor4oor4qUqKqUqKqUq\n"\
      "KqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKgAA/wAA/4or4oor4oor4qUqKqUq\n"\
      "KqUqKqUqKqUqKqUqKqUqKt64h964h964h964h964h964h6UqKqUqKqUqKqUq\n"\
      "KqUqKqUqKqUqKgAA/6UqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKt64h964\n"\
      "h964h964h964h964h964h964h964h964h6UqKqUqKqUqKqUqKgAA/6UqKqUq\n"\
      "KqUqKqUqKqUqKqUqKqUqKqUqKt64h964h964h964h964h964h964h964h964\n"\
      "h964h964h964h964h6UqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKt64\n"\
      "h964h964h964h964h964h964h964h964h964h964h964h964h964h964h964\n"\
      "h6UqKqUqKqUqKqUqKqUqKqUqKqUqKqUqKt64h964h964h964h964h964h964\n"\
      "h964h964h964h964h964h964h964h964h964h964h6UqKqUqKqUqKqUqKqUq\n"\
      "KqUqKt64h964h964h964h964h964h964h964h964h964h964h964h964h964\n"\
      "h964h964h964h964h6UqKqUqKqUqKqUqKqUqKqUqKt64h964h964h964h964\n"\
      "h964h964h964h964h964h964h964h964h964h964h964h964h964h6UqKqUq\n"\
      "KqUqKqUqKqUqKt64h964h964h964h964h964h964h964h964h964h964h964\n"\
      "h964h964h964h964h964h964h964h6UqKqUqKqUqKqUqKqUqKt64h964h964\n"\
      "h964h964h964h964h964h964h964h964h964h964h964h964h964h964h964\n"\
      "h964h6UqKqUqKqUqKqUqKt64h964h964h964h964h964h964h964h964h964\n"\
      "h964h964h964h964h964h964h964h964h964h964h6UqKqUqKqUqKqUqKt64\n"\
      "h964h964h964h964h964h964h964h964h964h964h964h964h964h1+eoF+e\n"\
      "oF+eoF+eoF+eoN64h6UqKqUqKqUqKqUqKt64h964h964h964h964h964h964\n"\
      "h964h964h964h964h964h964h1+eoF+eoF+eoF+eoF+eoF+eoF+eoKUqKqUq\n"\
      "KqUqKqUqKt64h964h964h964h964h964h964h964h964h964h964h964h964\n"\
      "h1+eoF+eoF+eoF+eoF+eoF+eoF+eoKUqKqUqKqUqKqUqKt64h964h964h964\n"\
      "h964h964h964h964h964h964h964h964h964h1+eoF+eoF+eoF+eoF+eoF+e\n"\
      "oKUqKqUqKqUqKqUqKqUqKt64h964h964h964h964h964h964h964h964h964\n"\
      "h964h964h964h1+eoF+eoF+eoF+eoF+eoF+eoKUqKqUqKqUqKt64h964h1+e\n"\
      "oF+eoF+eoF+eoF+eoN64h964h964h964h964h964h964h964h1+eoF+eoF+e\n"\
      "oF+eoF+eoKUqKqUqKt64h964h964h964h1+eoF+eoF+eoF+eoF+eoF+eoF+e\n"\
      "oN64h964h964h964h964h964h964h1+eoF+eoF+eoF+eoKUqKoor4t64h964\n"\
      "h964h964h1+eoF+eoF+eoF+eoF+eoF+eoF+eoF+eoN64h964h964h964h964\n"\
      "h964h964h964h964h4or4oor4oor4t64h964h964h964h1+eoF+eoF+eoF+e\n"\
      "oF+eoF+eoF+eoF+eoN64h964h964h964h964h964h964h964h4or4oor4oor\n"\
      "4oor4t64h964h964h964h1+eoF+eoF+eoF+eoF+eoF+eoF+eoF+eoF+eoN64\n"\
      "h964h964h964h964h964h964h4or4oor4oor4oor4g==\n"
    save_blob = Base64.decode64(saved_b64)

    assert test_blob, save_blob
  end
end
