# frozen_string_literal: true

require_relative 'circule/version'

require 'uri'
require 'net/http'
require 'chunky_png'
require 'securerandom'

# Convert a hash to an icon.
class Circule
  class Error < StandardError; end

  def initialize(
    hex: nil, btc_block: nil,
    bit0: 0, step: 16, bitn: 255,
    canvas: 24, ox: 0, oy: ox
  ) @_int      = hex.to_i(16) if hex
    @btc_block = btc_block
    @bit0      = bit0
    @step      = step
    @bitn      = bitn
    @canvas    = canvas
    @ox        = ox
    @oy        = oy
  end

  def int
    @_int ||=
      if @btc_block
        Net::HTTP
        .get_response(
          URI("https://live.blockcypher.com/btc/block/#{@btc_block}/"))
        .header['location']
        .split('/')[-1]
      else
        SecureRandom.hex(32)
      end
      .to_i(16)
  end

  def slices
    @_slices ||=
      @bit0
      .step(@bitn, @step)
      .map { |n| int[n, 16] }
  end

  def circles
    @_circles ||=
      slices
      .map { |s| [ s[0, 5], s[5, 5], s[10, 6] ] }
  end

  def image
    @_image ||=
      ChunkyPNG::Image
      .new(@canvas, @canvas)
      .tap do |img|
        @canvas.times do |a|
          @canvas.times do |b|
            img[a, b] = ChunkyPNG::Color.html_color(
              ChunkyPNG::Color::PREDEFINED_COLORS.keys[
                circles
                .select { |x, y, r| (a+@ox-x)**2 + (b+@oy-y)**2 < r**2 }
                .size
              ]
            )
          end
        end
      end
  end
end
