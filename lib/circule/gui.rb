# frozen_string_literal: true

require_relative '../circule'

require 'digest'
require 'tempfile'

begin
  require 'glimmer-dsl-tk'
  require 'glimmer/data_binding/observer'
rescue LoadError
  warn "Could not find gem 'glimmer-dsl-tk' in locally installed gems."
end

# Glimmer Tk GUI for Circule
class Circule::GUI
  include Glimmer

  attr_accessor :hex, :bit0, :step, :bitn, :canvas, :ox, :oy, :scale

  def initialize
    initialize_defaults
  end

  def initialize_defaults
    @hex    = ARGV[0] || SecureRandom.hex(32)
    @bit0   = 0
    @step   = 16
    @bitn   = 255
    @canvas = 24
    @ox     = 0
    @oy     = 0
    @scale  = 1
  end

  def observe_image_attribute_changes
    observer = Glimmer::DataBinding::Observer.proc { generate_image }
    observer.observe(self, :hex)
    observer.observe(self, :bit0)
    observer.observe(self, :step)
    observer.observe(self, :bitn)
    observer.observe(self, :canvas)
    observer.observe(self, :ox)
    observer.observe(self, :oy)
    observer.observe(self, :scale)
  end

  def generate_image
    tfile = Tempfile.new(%w[circule- .png])

    image = Circule.new(
      hex:    @hex,
      bit0:   @bit0.to_i,
      step:   @step.to_i,
      bitn:   @bitn.to_i,
      canvas: @canvas.to_i,
      ox:     @ox.to_i,
      oy:     @oy.to_i,
    ).image

    width  = image.width * @scale.to_i
    height = image.height * @scale.to_i

    image.resize(width, height).save(tfile.path)

    @image_label.image = tfile.path
  end

  def create_gui
    @root ||= root {
      title 'Circule GUI'

      frame {
        entry   {                   text <=> [self, :hex]     }

        label   {                   text            'bit0:'   }
        spinbox { from   0; to 255; text <=> [self, :bit0]    }
        label   {                   text            'step:'   }
        spinbox { from   4; to  64; text <=> [self, :step]    }
        label   {                   text            'bitn:'   }
        spinbox { from   0; to 255; text <=> [self, :bitn]    }
        label   {                   text            'canvas:' }
        spinbox { from  24; to  96; text <=> [self, :canvas]  }
        label   {                   text            'ox:'     }
        spinbox { from -96; to  96; text <=> [self, :ox]      }
        label   {                   text            'oy:'     }
        spinbox { from -96; to  96; text <=> [self, :oy]      }
        label   {                   text            'scale:'  }
        spinbox { from   1; to  24; text <=> [self, :scale]   }

        frame {
          @image_label = label { anchor 'center' }
        }

        button { text 'Open'; command { open    } }
        button { text 'Save'; command { save    } }
        button { text 'Exit'; command { exit(0) } }
      }
    }
  end

  def open(source = nil)
    source   ||= send(:get_open_file, parent: root)
    self.hex   = Digest::SHA256.hexdigest File.read(source)
  end

  def save(target = nil)
    source   = @image_label.image
    target ||= send(:get_save_file, parent: @root)
    source.write(target)
  end

  def launch
    observe_image_attribute_changes
    create_gui
    generate_image
    @root.open
  end
end
